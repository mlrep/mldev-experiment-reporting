# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


import evaluate
import pandas as pd
import time
import sys


if __name__ == "__main__":

    rdf_with_refs = sys.argv[1]
    output_metrics = sys.argv[2]

    meteor = evaluate.load('meteor')
    # bertscore = evaluate.load("bertscore")
    bleu = evaluate.load("bleu")

    df = pd.read_csv(rdf_with_refs)

    results = ['meteor: ' + str(meteor.compute(predictions=df['generated_text'], references=df['TEXT'].tolist())),
               'bleu: ' + str(bleu.compute(predictions=df['generated_text'], references=df['TEXT'].tolist())),
               # 'bertscore: ' + str(bertscore.compute(predictions=df['generated_text'], references=df['TEXT'].tolist(), lang="en"))
               ]

    with open(output_metrics, "w") as file:
        for result in results:
            file.write(result + '\n')