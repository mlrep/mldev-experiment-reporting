# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Gentext tag
========

Tag to generate text based on the object model or/and JSON.

* :py:class:`GenText`
    Basic class to generate text.
"""

from transformers import T5Tokenizer, T5ForConditionalGeneration
from mldev.experiment import experiment_tag
from mldev.expression import Expression
from mldev import logger
from experiment_reporting.json_query import JsonQuery
from experiment_reporting.dict2rdf import dict2rdf
from experiment_reporting.dict2rdf import batched
from experiment_reporting.node_decorator import NodeDecorator
import pandas as pd
import os

TEXT_GENERATIVE_MODEL = "TEXT_GENERATIVE_MODEL"


def attributes_extraction(object):
    """
    Fetch attributes from python object.

    :param object: python object
    :return: dictionary of object's attributes
    """

    attribute_dict = {}

    def extract(py_object, dict):
        if hasattr(py_object, '__dict__'):
            if isinstance(py_object, Expression):
                dict = str(py_object)
            else:
                dict = py_object.__dict__
        else:
            for k in dir(py_object):
                if k[0] != '_' and (repr(getattr(py_object, k))[0] != '<'):
                    if isinstance(getattr(py_object, k), Expression):
                        dict[k] = str(getattr(py_object, k))
                    else:
                        dict[k] = getattr(py_object, k)
        return dict

    return extract(object, attribute_dict)


@experiment_tag()
class GenText:
    """
    A class for generating text .

    Based on given items, a list of python objects and/or JsonQuery instances, a  dictionary of object attributes builds.
    The resulting dictionary is used to form RDF triplets. Then, the language model generates the text by RDF triplets.

    :param items: a list of objects ond/or JsonQuery instances
    :param output_file: a .csv file where generated text will be stored
    """

    def __init__(self, items=[], output_file=None):
        self.__output = output_file
        self.__text = str()
        self.__items = items

    def __call__(self, *args, **kwargs):
        """Execute `GenText`.

        This method generates a text using pretrained text generation model.
        """
        if TEXT_GENERATIVE_MODEL in os.environ:
            self.model = T5ForConditionalGeneration.from_pretrained(os.environ[TEXT_GENERATIVE_MODEL])
            self.tokenizer = T5Tokenizer.from_pretrained(os.environ[TEXT_GENERATIVE_MODEL])
        else:
            self.model = T5ForConditionalGeneration.from_pretrained("Mugadzhir/T5_small_webnlg")
            self.tokenizer = T5Tokenizer.from_pretrained("Mugadzhir/T5_small_webnlg")

        rdf_with_generated_text = {"RDF": [],
                                   "TEXT": []}
        for string_of_rdfs in self.__items2rdf():
            gen_text = str(self.generate(string_of_rdfs))
            self.__text += gen_text + ' '
            if self.__output:
                rdf_with_generated_text['RDF'].append(string_of_rdfs)
                rdf_with_generated_text['TEXT'].append(gen_text)
        if self.__output:
            self.__save_to_csv(rdf_with_generated_text)

    def __items2rdf(self):
        """Processing given items of python objects and/or JsonQuery instances and forming RDF trimplets"""

        rdf = []
        for item in self.__items:
            if isinstance(item, NodeDecorator):
                if item.name is None:
                    raise ValueError("NodeDecorator name is not defined.")
                object_attr_dict = attributes_extraction(item.node)
                logger.debug('---------------Attributes RDFs-------------')
                logger.debug(object_attr_dict)
                object_attr_dict_upper = {item.name: object_attr_dict}
                for batch in self.__form_list_rdfs(object_attr_dict_upper):
                    rdf.append(' && '.join(batch))

            elif isinstance(item, JsonQuery):
                for batch in self.__form_list_rdfs(item()):
                    rdf.append(' && '.join(batch))
            else:
                object_attr_dict = attributes_extraction(item)
                object_attr_dict_upper = {}
                if 'name' not in object_attr_dict.keys():
                    object_attr_dict_upper[item.__class__.__name__] = object_attr_dict
                else:
                    object_attr_dict_upper[object_attr_dict["name"]] = object_attr_dict
                for batch in self.__form_list_rdfs(object_attr_dict_upper):
                    rdf.append(' && '.join(batch))

        logger.debug('---------------Final RDFs-------------')
        logger.debug(rdf)

        return rdf

    @staticmethod
    def __form_list_rdfs(dict):
        return batched(list(filter(lambda k: ('ROOT' not in k) and ('{}' not in k) and ('[]' not in k)
                                             and ('<' not in k) and ('>' not in k), dict2rdf(dict))), 3)

    def generate(self, rdf):
        """Generate text using transformers library"""

        self.model.eval()
        input_ids = self.tokenizer.encode("WebNLG:{} ".format(rdf), return_tensors="pt")
        outputs = self.model.generate(input_ids, max_length=200)
        gen_text = self.tokenizer.decode(outputs[0]).replace('', '').replace('', '').replace('<pad>', '').replace(
            '</s>', '')

        return gen_text

    def __save_to_csv(self, dict):
        if not os.path.exists(os.path.dirname(str(self.__output))):
            os.makedirs(os.path.dirname(str(self.__output)), exist_ok=False)
            os.utime(os.path.dirname(str(self.__output)), None)
        file_df = pd.DataFrame.from_dict(dict)
        file_df.to_csv(str(self.__output), index=False)

    def __repr__(self):
        return self.__text
