# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


def iterate_dict(d, parents=[]):
    """
    Iterates over one dict. Usefull for looping through a multidimensional dictionary.

    :param d: dictionary.
    :param parents: key for parent element.
    :return: list of tuples: (key, value, parent_keys)
    """
    r = []
    for k, v in d.items():
        if isinstance(v, dict):
            r.extend(iterate_dict(v, parents + [k]))
        elif isinstance(v, list):
            r.append((k, v, parents))
        else:
            r.append((k, v, parents))

    return r