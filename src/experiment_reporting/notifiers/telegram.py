# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
TelegramNotifier tag
========

Tag to send notifications to Telegram.

* :py:class:`TelegramNotifier`
    Basic class to send notifications.
"""

import json
import os
import requests

from jinja2 import FileSystemLoader, Environment, BaseLoader

from mldev.experiment_tag import experiment_tag


requests.packages.urllib3.disable_warnings(
    requests.packages.urllib3.exceptions.InsecureRequestWarning
)


def _send_message(message: str, chat_id: str, api_key: str):
    headers = {
        "Content-Type": "application/json",
        "Proxy-Authorization": "Basic base64",
    }
    data_dict = {
        "chat_id": chat_id,
        "text": message,
        "parse_mode": "HTML",
        "disable_notification": True,
    }
    data = json.dumps(data_dict)
    url = f"https://api.telegram.org/bot{api_key}/sendMessage"
    try:
        response = requests.post(url, data=data, headers=headers, verify=False)
        response.raise_for_status()
    except requests.HTTPError:
        raise Exception(f"Error while sending a telegram message.")
    except requests.Timeout:
        raise Exception(f"Telegram request took too long.")
    return response


def _send_media_group(chat_id, api_key, files, filetype):
    url = f"https://api.telegram.org/bot{api_key}/sendMediaGroup"
    media = []
    request_files = {}
    for file_path in files:
        name = os.path.basename(file_path)
        request_files[name] = open(file_path, "rb").read()
        media.append(dict(type=filetype, media=f"attach://{name}"))
    try:
        response = requests.post(
            url,
            data={
                "chat_id": chat_id,
                "media": json.dumps(media),
            },
            files=request_files,
        )
        response.raise_for_status()
    except requests.HTTPError:
        raise Exception(f"Error while sending a telegram message.")
    except requests.Timeout:
        raise Exception(f"The Telegram request took too long.")
    return response


def _split_files_by_media_groups(files):
    photos = []
    documents = []
    for f in files:
        name = os.path.basename(f)
        if name.lower().endswith((".png", ".jpg", ".jpeg", ".tiff", ".bmp", ".gif")):
            photos.append(f)
        elif name.lower().endswith((".html", ".pdf")):
            documents.append(f)
        else:
            raise Exception("Unsupported file type.")
    return {
        "photo": photos,
        "document": documents,
    }


@experiment_tag()
class TelegramNotifier:
    """
    The class for sending notifications to Telegram.

    :param api_key: https://core.telegram.org/api/obtaining_api_id
    :param chat_id: the chat id of the Telegram chat
    :param context: the context used for templates
    :param message_template: the Jinja template
    :param base_template_path: a path to the report template (e.g., from !Report)
    :param attached: some files for attaching to the message
    """

    def __init__(
        self,
        api_key: str,
        chat_id: str,
        context: dict = None,
        message_template: str = None,
        base_template_path: str = None,
        attached: list = None,
    ):
        self._api_key = api_key
        self._chat_id = chat_id
        if context is None:
            self._context = {}
        else:
            self._context = context
        self._message_template = message_template
        self._base_template_path = base_template_path
        self._attached = attached

    def __call__(self, *args, **kwargs):
        """
        This method generates and sends a telegram message by rendering a template.
        """

        api_key = str(self._api_key)
        if not api_key or api_key == "None":
            raise Exception("Please set the Telegram API key.")
        chat_id = str(self._chat_id)

        if self._base_template_path:
            base_template_path, base_template_name = os.path.split(
                str(self._base_template_path)
            )
            env = Environment(loader=FileSystemLoader(searchpath=base_template_path))
            template = env.get_template(base_template_name)

            for block_name in template.blocks:
                block = template.blocks[block_name]
                lines = []
                for line in block(template.new_context(self._context)):
                    lines.append(line)
                block_value = "".join(lines)
                self._context[block_name] = block_value

        if self._message_template:
            message_template = str(self._message_template)
            telegram_template = Environment(loader=BaseLoader()).from_string(
                message_template
            )
            report = telegram_template.render(self._context)
            _send_message(report, chat_id, api_key)

        if self._attached:
            attached = [str(a) for a in self._attached]
            for filetype, files in _split_files_by_media_groups(attached).items():
                _send_media_group(chat_id, api_key, files, filetype)
