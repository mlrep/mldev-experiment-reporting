# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


def dict2rdf(obj: dict) -> list:
    """
    Form a list of rdf triples by given dictionary of object's attributes.

    :param obj: dictionary of object's attributes.
    :return: list of rdf triples
    """
    arr = []

    def extract(obj, arr, parent="ROOT"):
        """
        Recursively search for values in DICT tree.

        :param obj: dictionary of object's attributes.
        :param arr: list to store extracted rdf triples.
        :return: list of rdf triples
        """
        for k, v in obj.items():
            if isinstance(v, list) and bool(v):
                for item in v:
                    if isinstance(item, dict) and bool(item):
                        if "name" in item.keys():
                            arr.append(str(k) + " | " + "have" + " | " + str(item["name"]))
                        extract(item, arr, k)
                    else:
                        arr.append(str(parent) + " | " + str(k) + " | " + str(item))
            elif isinstance(v, dict) and bool(v):
                for sub_k, sub_v in v.items():
                    if isinstance(sub_v, dict) and bool(sub_v):
                        arr.append(str(k) + " | " + "have" + " | " + str(sub_k))
                        extract(sub_v, arr, sub_k)
                    elif isinstance(sub_v, list) and bool(sub_v):
                        for item in sub_v:
                            if isinstance(item, dict) and bool(item):
                                arr.append(str(k) + " | " + "have" + " | " + str(sub_k))
                                extract(item, arr, sub_k)
                            else:
                                arr.append(str(k) + " | " + str(sub_k) + " | " + str(item))
                    else:
                        arr.append(str(k) + " | " + str(sub_k) + " | " + str(sub_v))
            elif not isinstance(v, (dict, list)) and bool(v):
                arr.append(str(parent) + " | " + str(k) + " | " + str(v))
        return arr

    return extract(obj, arr)


def batched(iterable, n=1):
    """
    Split iterable object to parts size of n.

    :param iterable: iterable object.
    :param n: size of batch.
    :return: batched iterable object.
    """
    length = len(iterable)
    for ndx in range(0, length, n):
        yield iterable[ndx:min(ndx + n, length)]
