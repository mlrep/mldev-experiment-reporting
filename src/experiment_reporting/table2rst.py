# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Table2Rst tag
========

Tag for reading and filtering tabular data.

* :py:class:`Table2Rst`
    Class for reading and filtering tabular data.
"""

import pandas as pd
import os
from mldev.experiment import experiment_tag
from tabulate import tabulate


@experiment_tag()
class Table2Rst:
    """
    A class for reading and filtering tabular data.

    A given tabular data's file(``.parquet`` or ``.csv``) is reading and, if needed, going through two filters,
    then the dataframe is converting to ``rst`` table format and passing to ``frame``.

    To substitute the table in the template you need:
    - Include by YAML link `*` read data (as an instance of tag `!Table2Rst`) in the report data model in tag `!Report`.
    - Use in the template rst-method `table`.
    - In rst-method `table` pass data from report data model as a parameter. Jinja's expressions are used for this,
    the data is available through the instance of the `!Table2Rst` tag.

    :param path: a path to tabular data's file
    :param columns_filters: common with `pandas.filter`
    :param rows_filters: a tuple of key-value pairs to filter on
    """
    def __init__(self,
                 path,
                 columns_filters=None,
                 rows_filters=None,
                 ):

        self.__path, self.__name = os.path.split(path)
        self.__root, self.__ext = os.path.splitext(self.__name)
        self.__columns_filters = columns_filters
        self.__rows_filters = rows_filters

    def __call__(self):
        if self.__ext == '.parquet':
            self.data = self.__parquet_table()
        elif self.__ext == '.csv':
            self.data = self.__csv_table()
        else:
            raise TypeError("Invalid file type. Only allowed Csv and Parquet")
        self.frame = tabulate(self.__processing(self.data), headers='keys', tablefmt='rst', stralign='center')

    def __pd_filter(self, data):
        """Filtering  columns using `pandas.filter`.

        """
        data = data.filter(items=self.__columns_filters['items'] if "items" in self.__columns_filters else None,
                           regex=self.__columns_filters['regex'] if "regex" in self.__columns_filters else None,
                           like=self.__columns_filters['like'] if "like" in self.__columns_filters else None,
                           axis=self.__columns_filters['axis'] if "axis" in self.__columns_filters else None)
        return data

    def __pd_locate(self, data):
        """Filtering rows using `pandas.loc`.

        """
        for single_filter in self.__rows_filters:
            data = data.loc[data[single_filter['column_name']] == single_filter['values']]
        return data

    def __csv_table(self):
        """Reading CSV file and filtering data it contains

        """
        data = pd.read_csv(os.path.join(str(self.__path), str(self.__name)), index_col=0)
        return data

    def __parquet_table(self):
        """Reading Parquet file and filtering data it contains

        """
        data = pd.read_parquet(os.path.join(str(self.__path), str(self.__name)))
        return data

    def __processing(self, data):
        """Filtering data.

        """
        if self.__rows_filters:
            data = self.__pd_locate(data)
        if self.__columns_filters:
            data = self.__pd_filter(data)
        return data

    def __repr__(self):
        return self.frame
