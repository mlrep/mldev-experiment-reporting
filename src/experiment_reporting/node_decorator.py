# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
NodeDecorator tag
========

Tag to decorate nodes with additional attributes.

* :py:class:`GenText`
    Basic class to decorate nodes.
"""

from mldev.experiment import experiment_tag


@experiment_tag()
class NodeDecorator:
    """
    A class for decorating nodes.

    :param node: a node to decorate
    :param name: an additional attribute containing a name
    """
    def __init__(self, node, name):
        self.node = node
        self.name = name

