# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Report tag
========

Tag to generate reports.

* :py:class:`Report`
    Basic class to generate reports.
"""

import os
import pathlib
import shutil
import tempfile
from typing import Iterable

from jinja2 import FileSystemLoader, Environment

from mldev.experiment import FilePath
from mldev.experiment_tag import experiment_tag
from experiment_reporting.iterate_dict import iterate_dict
from experiment_reporting.json_query import JsonQuery
from experiment_reporting.table2rst import Table2Rst
from experiment_reporting.gentext import GenText


STATIC_DIR = "_static"


@experiment_tag()
class Report:
    """
    A class for generating reports.

    The `Report` class facilitates the creation of reports by combining a report data model and a report template.
    The report data model specifies the dynamic content of the report, which depends on the experiment specification
    and results. It is defined in the `report_model` element of the `!Report` tag instance, representing the MLDev stage
    of the experiment. The report model can be any valid YAML object.

    Elements of the experiment without state (their specifications) can be added to the report through the report
    data model. However, experiment elements with state (e.g., MLDev stage) need to be added to the report by reading
    JSON files containing saved state and parameters.

    The report template defines the report's structure and includes static information, such as report text,
    that remains independent of the experiment runs. The template is written in reStructuredText format.

    :param name: The name of the report.
    :param output: The folder where the generated report will be stored.
    :param template: The path to the report template.
    :param report_model: The report's data model.
    :param conf: The configuration file path used for report conversion (optional).
    :param pdf_output: The folder where the generated PDF report will be stored (optional).
    :param html_output: The folder where the generated HTML report will be stored (optional).

    .. note::
        If both `output` and `template` are not specified, an Exception will be raised.

    The `generate_report` method is used to render the template and create the report in reStructuredText format.

    The `convert_report` method is used to convert the RST report into other formats such as PDF and HTML,
    based on the specified output formats (`pdf_output` and `html_output`).
    """
    def __init__(self, name,
                 output=None,
                 template=None,
                 report_model=None,
                 conf=None,
                 pdf_output=None,
                 html_output=None,
                 ):

        if output is None and template is None:
            raise Exception('At least one of the arguments output or template '
                            'must be specified.')
        if output is None and pdf_output is None and html_output is None:
            raise Exception('At least one of output formats '
                            'must be specified.')
        self.__remove_output = False
        if output is None:
            self.__remove_output = True
            output = tempfile.TemporaryDirectory().name

        if report_model is None:
            report_model = {}
        self.__name = name
        self.__output = output
        if template:
            self.__template_path, self.__template_name = os.path.split(str(template))
        else:
            self.__template_path, self.__template_name = None, None
        self.report_model = report_model
        self.__conf = conf
        self.__pdf_output = pdf_output
        self.__html_output = html_output

    def __call__(self, *args, **kwargs):
        """Execute `Report`.

        This method generates a report by rendering template with data model.
        """

        for k, v, parents in iterate_dict(self.report_model):
            if isinstance(v, (JsonQuery, Table2Rst, GenText)):
                v()
        rst_file_path = os.path.join(str(self.__output), str(self.__name) + ".rst")

        if self.__template_path and self.__template_name:
            self.generate_report(
                path=rst_file_path,
                template=self.__template_name,
            )

        if self.__pdf_output or self.__html_output:
            self.convert_report(
                rst_file_path=rst_file_path,
                conf=str(self.__conf),
                pdf_output=str(self.__pdf_output) if self.__pdf_output else None,
                html_output=str(self.__html_output) if self.__html_output else None,
            )

        if self.__remove_output:
            shutil.rmtree(self.__output)

    def generate_report(self, path, template):
        """This method renders template.

        """

        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path), exist_ok=False)
            os.utime(os.path.dirname(path), None)

        env = Environment(
            loader=FileSystemLoader(searchpath=self.__template_path)
        )
        template = env.get_template(template)

        with open(path, 'w') as f:
            f.write(template.render(self.report_model,
                                    output=self.__output
                                    ))

    def convert_report(self, rst_file_path, conf, pdf_output, html_output):
        """
        Convert the RST (reStructuredText) report to other formats like PDF and HTML.

        The `convert_report` method uses the `sphinx-build` tool to convert the RST report to the specified formats.

        :param str rst_file_path: The path to the RST report file to be converted.
        :param str conf: The path to the configuration file used for report conversion.
        :param str pdf_output: The folder path where the generated PDF report will be stored.
            Pass None to skip PDF conversion.
        :param str html_output: The folder path where the generated HTML report will be stored.
            Pass None to skip HTML conversion.

        If the `pdf_output` is provided, the RST report will be converted to PDF and saved in the specified folder.
        If the `html_output` is provided, the RST report will be converted to HTML and saved in the specified folder.

        .. note::
            Ensure that the `sphinx-build` tool is installed and available in the system's PATH.

        .. note::
            The configuration file should be a valid Sphinx configuration file (e.g., `conf.py`).

        .. warning::
            If both `pdf_output` and `html_output` are set to None, no conversion will be performed,
            and no exceptions will be raised.
        """

        with tempfile.TemporaryDirectory() as rst_dir:
            rst_file_name = os.path.basename(rst_file_path)
            shutil.copyfile(rst_file_path, os.path.join(rst_dir, rst_file_name))
            config_file_name = os.path.basename(conf)
            shutil.copyfile(conf, os.path.join(rst_dir, config_file_name))

            # copy static files
            static_dir = os.path.join(rst_dir, STATIC_DIR)
            os.mkdir(static_dir)

            def find_pathes(iterable: Iterable):
                res = []
                for i in iterable:
                    if isinstance(i, FilePath):
                        res.append(i)
                    elif isinstance(i, list) or isinstance(i, set):
                        res.extend(find_pathes(i))
                    elif isinstance(i, dict):
                        res.extend(find_pathes(i.values()))
                return res

            for path in find_pathes(self.report_model):
                src_path = str(path)
                dst_path = os.path.join(static_dir, os.path.basename(src_path))
                shutil.copyfile(src_path, dst_path)

            # convert from the rst
            if html_output:
                command = f"sphinx-build -b html {rst_dir} {html_output}"
                os.system(command)
            if pdf_output:
                with tempfile.TemporaryDirectory(dir=rst_dir) as result_dir:
                    command = f"sphinx-build -M latexpdf {rst_dir} {result_dir}"
                    os.system(command)

                    pathlib.Path(pdf_output).mkdir(parents=True, exist_ok=True)

                    latex_dir = os.path.join(result_dir, "latex")
                    for pdf_file in pathlib.Path(latex_dir).glob("*.pdf"):
                        pdf_file_name = os.path.basename(pdf_file)
                        src_path = os.path.join(latex_dir, pdf_file_name)
                        dst_path = os.path.join(pdf_output, pdf_file_name)
                        shutil.copyfile(src_path, dst_path)
