# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
JSON tag
========

Tag for reading and querying JSON.

* :py:class:`Json`
    Class for reading and querying JSON.
"""

import os
from mldev.experiment import experiment_tag
import json
import jmespath


@experiment_tag()
class JsonQuery:
    """
    A class for reading and querying JSON.

    To substitute the data in the template you need:

    Include by YAML link `*` read data (as an instance of the `!Json` tag) in the report data model in the `!Report` tag.
    Add data from the report data model to the template. Jinja's expressions are used for this,
    the data is available through the instance of the `!Json` tag.

    :param path: a path to JSON file
    :param json_query: a query in JMESPath notation
    """
    def __init__(self,
                 path,
                 query="Default"):

        self.__path, self.__name = os.path.split(str(path))
        self.__json_query = query

    def __repr__(self):
        return str(self.json_results)

    def __json_load(self):
        with open(os.path.join(str(self.__path), str(self.__name)), 'r') as json_file:
            self.__json_object = json.load(json_file)
        return self.__json_object

    def __query_json(self):
        return jmespath.compile(self.__json_query).search(self.__json_load())

    def __call__(self):
        if self.__json_query != "Default":
            self.json_results = self.__query_json()
        else:
            self.json_results = self.__json_load()
        return self.json_results
