#### Описание MLDev Experiment Reporting на русском языке доступно по [ссылке](https://gitlab.com/mlrep/mldev-experiment-reporting/-/blob/master/README-ru.md) и в русскоязычных статьях [wiki](https://gitlab.com/mlrep/mldev-experiment-reporting/-/wikis/Главная)

# MLDev Experiment reporting

This package is designed to speed up the exchange of information.

For this purpose, it implements a report generator for an experiment conducted in the Mldev system, based on a template in restructuredText format.

The generator reduces report generation time, which improves communication between researchers

# Documentation

User documentation is available in project [wiki](https://gitlab.com/mlrep/mldev-experiment-reporting/-/wikis/Home).

## User guide

[User guide](https://gitlab.com/mlrep/mldev-experiment-reporting/-/wikis/User-Guide) helps with:

- first run of report generation
- learning of MLDev Experiment reporting functions

# Partners and supporters

### FASIE - Foundation for Assistance to Small Innovative Enterprises

<p>
<a href="https://fasie.ru/"><img src="https://fasie.ru/local/templates/.default/markup/img/FASIE_EN_logo.png" alt="Foundation for Assistance to Small Innovative Enterprises" height="80px"/></a>
</p>

### Gitlab open source

<p>
<a href="https://about.gitlab.com/solutions/open-source/"><img src="https://gitlab.com/mlrep/mldev/-/wikis/images/gitlab-logo-gray-stacked-rgb.png" alt="GitLab Open Source program" height="80px"></a>
</p> 


# Support and contacts

Give feedback, suggest feature, report bug:
- [Telegram](https://t.me/mldev_betatest) user group
- [#mlrep](https://opendatascience.slack.com) channel at OpenDataScience Slack
- [Gitlab](https://gitlab.com/mlrep/mldev-experiment-reporting/-/issues) issue tracker

# Contributing

Please check the [CONTRIBUTING.md](https://gitlab.com/mlrep/mldev/-/blob/develop/CONTRIBUTING.md) guide if you'd like to participate in the project, ask a question or give a suggestion.

# License

The software is licensed under [Apache 2.0 license](LICENSE).


