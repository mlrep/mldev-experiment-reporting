# MLDev Experiment reporting

Данный пакет предназначен для ускорения обмена информацией.

Для этого реализуется генератор отчета об эксперименте, проведенном в системе Mldev, по шаблону в формате restructuredText.

Генератор позволяет уменьшить время построения отчетов по эксперименту, что улучшает коммуникацию между исследователями

# Документация

Документация пользователя доступна в [wiki](https://gitlab.com/mlrep/mldev-experiment-reporting/-/wikis/Home) проекта.

## Руководство пользователя

[Руководство пользователя](https://gitlab.com/mlrep/mldev-experiment-reporting/-/wikis/%D0%A0%D1%83%D0%BA%D0%BE%D0%B2%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE-%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8F) поможет:

- Запустить простейшую генерацию
- изучить функции Mldev Experiment reporting

# Партнеры и поддержка

### Фонд Содействия Инновациям

<p>
<a href="https://fasie.ru/"><img src="https://fasie.ru/local/templates/.default/markup/img/logo_new.svg" alt="Фонд Содействия Инновациям" height="80px"/></a>
</p>

### Gitlab open source

<p>
<a href="https://about.gitlab.com/solutions/open-source/"><img src="https://gitlab.com/mlrep/mldev/-/wikis/images/gitlab-logo-gray-stacked-rgb.png" alt="GitLab Open Source program" height="80px"></a>
</p> 

# Участие

Чтобы участвовать в проекте, задать вопрос или предложить функцию, пожалуйста, ознакомьтесь с руководством [CONTRIBUTING.md](https://gitlab.com/mlrep/mldev/-/blob/develop/CONTRIBUTING.md). 

# Поддержка и контакты

Предоставить обратную связь, предложить функцию или сообщить о баге можно:
- Группа пользователей в [Телеграм](https://t.me/mldev_betatest)
- Канал [#mlrep](https://opendatascience.slack.com) в Slack OpenDataScience
- Трэкер задач [Gitlab](https://gitlab.com/mlrep/mldev-experiment-reporting/-/issues)

# Лицензия

Это программное обеспечение распространятся с лицензией [Apache 2.0 license](LICENSE)

