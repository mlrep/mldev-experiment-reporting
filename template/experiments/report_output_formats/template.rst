=================================================================================================
Влияние фиксированного шума на алгоритм {{ params.json_results.algorithm.name }}
=================================================================================================

.. _code:

*Введение*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
{% block telegram_text -%}
Проблема многорукого бандита хорошо изучена для решения реальных задач.
В этой задаче агент многократно выбирает действие и получает обратную связь в виде
стохастического вознаграждения. Цель агента - максимизировать кумулятивное вознаграждение
с течением времени путем определения оптимального действия, которое имеет максимальное
вознаграждение.
{%- endblock telegram_text %}

На практике разработчики рекомендательных систем нередко рассматривают вознаграждения
как истинные значения, свободные от шума, и не используют никаких методов для борьбы
с ним. Тем не менее наличие зашумленных наград из-за внешних факторов, таких как
человеческие ошибки, неопределенность, присущая человеку непоследовательность и т.д.,
может привести к неправильным рекомендациям. Такая проблема в рекомендательных
системах известна как естественный шум (Natural Noise) и может негативно влиять на
рекомендации, что приводит к ухудшению пользовательского опыта.

{% block telegram_text_2 -%}
В данной работе рассматривается влияние зашумленности обратной связи на алгоритм
{{ params.json_results.algorithm.name }}.
{%- endblock telegram_text_2 %}

*Задача эксперимента*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Показать влияние шума на алгоритм {{ params.json_results.algorithm.name }}.

*Методика*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
В данной работе рассматривается случай с равномерным и ограниченным шумом

.. math::
    ω ∼ Uniform[−w, w], w = {{ w }}

который добавляется к распределению наград:

.. code-block:: {{ params.json_results.bandit.functions.get_rewards_with_noise.language }}

    {{ params.json_results.bandit.functions.get_rewards_with_noise.code | indent(4)}}

Общая схема взаимодействия алгоритма и бандита:

.. code-block:: {{ params.json_results.run.language }}

    {{ params.json_results.run.code | indent(4)}}


Параметры эксперимента:

- рассматриваемые значения w: {{ w }}
- количество раундов: {{ round_count }}

*Результаты*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. figure:: /{{ pictures.fig1.path.get_path() }}
    :align: center
    :scale: 100%

    Рис.1. {{ pictures.fig1.desc }}

*Выводы*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Для шума с :math:`w = 0.2` наблюдается влияние шума на ожидаемый регрет.
