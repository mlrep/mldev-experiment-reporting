import turtle
from math import cos, sin, tan, pi
from datetime import datetime
from PIL import Image
import os
import sys

window = turtle.Screen()
window.screensize(1000, 1000)
pen = turtle.Turtle()
pen.hideturtle()
pen.pensize(3)
pen.penup()

angle = 0
theta = 0.01
a = float(sys.argv[1]) #-50 # -7
b = float(sys.argv[2]) # -17
c = float(sys.argv[3])# 27
d = float(sys.argv[4])#-4
path = sys.argv[5]
print(a, b, c, d, path)
steps = int(250 * pi / theta)
turtle.tracer(0, 0)

for t in range(0, steps):
    angle += theta
    x = (-cos(a * angle / d) - cos(b * angle / d) + cos(c * angle / d)) * 150
    y = (-sin(a * angle / d) - sin(b * angle / d) + sin(c * angle / d)) * 150

    pen.goto(x, y)
    pen.pendown()
turtle.update()

date = (datetime.now()).strftime("%d%b%Y-%H%M%S")
path_file, name_file = os.path.split(path)
root_file, ext_file = os.path.splitext(name_file)

screen = turtle.Screen()
screen.setup(1000, 1000)
turtle = turtle.Turtle(visible=False)
screen.tracer(False)
screen.tracer(True)
canvas = screen.getcanvas()
canvas.postscript(file=path_file + "/" + root_file + '.eps', width=1000, height=1000)

img = Image.open(path_file+"/"+root_file + '.eps')
img.save(path)

print("it is done")
