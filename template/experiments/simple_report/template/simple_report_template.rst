=================================================================================================
{{  title  }}
=================================================================================================

.. contents::

*{{  paragraph.head  }}*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Сначала статический текст из шаблона. Текст далее вставлен из модели данных отчета: *{{  paragraph.head  }}*


**{{  math.head  }}**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Формула, вставленная из модели данных отчета:

.. math::
    {{  math.math  }}

