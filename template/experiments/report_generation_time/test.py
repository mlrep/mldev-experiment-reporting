# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

import time
import os


def test_report_time():
    t = time.time()
    os.system('mldev run -f tests/report_generation_time/experiment.yml')
    return time.time() - t


if __name__ == "__main__":
    print(f"Время генерации отчета {test_report_time()}  секунд")
