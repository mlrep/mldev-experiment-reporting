MLDev Experiment reporting
=================================

.. toctree::
   :hidden:

   self

This is the official documentation for the MLDev Experiment Reporting package. This package is designed to speed up the exchange of information.

For this purpose, it implements a report generator for an experiment conducted in the Mldev system, based on a template in restructuredText format.

The generator reduces report generation time, which improves communication between researchers.

Documentation
----------------------

User documentation is available in project `wiki <https://gitlab.com/mlrep/mldev-experiment-reporting/-/wikis/Home>`_

.. toctree::
   :maxdepth: 1
   :caption: User documentation

   mldev-experiment-reporting-tutorial
   mldev-experiment-reporting-user-guide

User documentation helps with:

- first run of report generation
- learning of MLDev Experiment reporting functions

Developer docs
--------------

.. toctree::
   :maxdepth: 1
   :caption: API Reference

   mldev-experiment-reporting-develop

Partners and supporters
-----------------------

**FASIE - Foundation for Assistance to Small Innovative Enterprises**

.. image:: https://www.fbras.ru/wp-content/uploads/2015/06/fasie_en__1_.png
   :alt: Foundation for Assistance to Small Innovative Enterprises
   :height: 80px
   :target: https://fasie.ru/

**Gitlab open source**

.. image:: https://gitlab.com/mlrep/mldev/-/wikis/images/gitlab-logo-gray-stacked-rgb.png
   :alt: GitLab Open Source program
   :height: 80px
   :target: https://about.gitlab.com/solutions/open-source/

Support and contacts
---------------------------------

Give feedback, suggest feature, report bug:
- `Telegram <https://t.me/mldev_betatest>`_ user group
- `#mlrep <https://opendatascience.slack.com>`_ channel at OpenDataScience Slack
- `Gitlab <https://gitlab.com/mlrep/mldev-experiment-reporting/-/issues>`_ issue tracker

Contributing
----------------------

Please check the `CONTRIBUTING.md<https://gitlab.com/mlrep/mldev/-/blob/develop/CONTRIBUTING.md>`_ guide if you'd like to participate in the project, ask a question or give a suggestion.

License
-----------

The software is licensed under `Apache 2.0 license <https://gitlab.com/mlrep/mldev/-/blob/develop/LICENSE>`_.

Index and tables
----------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
